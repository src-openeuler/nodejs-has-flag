%{?nodejs_find_provides_and_requires}
%global enable_tests 0
Name:           nodejs-has-flag
Version:        2.0.0
Release:        1
Summary:        Check if argv has a specific flag
License:        MIT
URL:            https://github.com/sindresorhus/has-flag.git
Source0:        https://registry.npmjs.org/has-flag/-/has-flag-%{version}.tgz
Source1:        https://raw.githubusercontent.com/sindresorhus/has-flag/v%{version}/test.js
ExclusiveArch:  %{nodejs_arches} noarch
BuildArch:      noarch
BuildRequires:  nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:  npm(ava)
%endif
Requires:       nodejs
%description
Check if argv has a specific flag.

%prep
%setup -q -n package
cp -p %{SOURCE1} .

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/has-flag
cp -pr package.json *.js \
  %{buildroot}%{nodejs_sitelib}/has-flag
%nodejs_symlink_deps

%check
%nodejs_symlink_deps --check
%{__nodejs} -e 'require("./")'
%if 0%{?enable_tests}
%__nodejs test.js
%endif

%files
%{!?_licensedir:%global license %doc}
%doc *.md
%license license
%{nodejs_sitelib}/has-flag

%changelog
* Tue Aug 11 2020 wangyue <wangyue92@huawei.com> - 2.0.0-1
- package init
